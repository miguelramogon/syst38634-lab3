package password;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

//	@Test
//	public void test() {
//		fail("Not yet implemented");
//	}
	
	@Test
	public void testCheckPasswordLength() {
		assertTrue("Invalid passowrd length", PasswordValidator.checkPasswordLength("thisIsAValidPassowrd") != true);
	}
	
	@Test
	public void testCheckTwoDigit() {
		assertTrue("Invalid passowrdLength", PasswordValidator.checkTwoDigit("2Thereare2") != true);
	}

}
