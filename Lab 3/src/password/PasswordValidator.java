package password;

public class PasswordValidator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public static boolean checkPasswordLength(String password) {
		return password.length() >= 8;
	}
	//Before
//	public static boolean checkTwoDigit(String password) {
//		int count = 0;
//		for(int i = 0; i < password.length(); i++) {
//			if(Character.isDigit(password.charAt(i))) {
//				count ++;
//			}
//		}
//		return count >= 2;
//	}
	
	//After
	public static boolean checkTwoDigit(String password) {
		int count = 0;
		for(int i = 0; i < password.length(); i++) {
			if(Character.isDigit(password.charAt(i))) {
				count ++;
			}
			if(count >= 2) {
				return true;
			}
		}
		return false;
	}

}
